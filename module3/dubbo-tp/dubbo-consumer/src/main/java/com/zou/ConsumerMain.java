package com.zou;

import com.zou.controller.HelloController;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ConsumerMain {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConsumerConfig.class);
        System.out.println("consumer start success");
        HelloController helloController = applicationContext.getBean(HelloController.class);

        // 30毫秒一次 -> 每分钟（60000毫秒）大约2000次
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);
        executorService.scheduleAtFixedRate(helloController::sayHello, 1, 30, TimeUnit.MILLISECONDS);
        executorService.scheduleAtFixedRate(helloController::sayHot, 1, 30, TimeUnit.MILLISECONDS);
        executorService.scheduleAtFixedRate(helloController::sayCold, 1, 30, TimeUnit.MILLISECONDS);
    }

    @Configuration
    @EnableDubbo(scanBasePackages = "com.zou.service")
    @PropertySource("classpath:/dubbo-consumer.properties")
    @ComponentScan("com.zou.controller")
    public static class ConsumerConfig {

    }
}
