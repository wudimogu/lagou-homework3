package com.zou.controller;

import com.zou.service.HelloService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Component;

@Component
public class HelloController {

    @DubboReference(timeout = 200)
    private HelloService helloService;

    public String sayHello() {
        return helloService.sayHello();
    }

    public String sayCold() {
        return helloService.sayCold();
    }

    public String sayHot() {
        return helloService.sayHot();
    }
}
