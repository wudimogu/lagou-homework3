package com.zou.service.impl;

import com.zou.service.HelloService;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@DubboService
public class HelloServiceImpl implements HelloService {

    public static final Random RANDOM = new Random();

    void pre() {
        try {
            TimeUnit.MILLISECONDS.sleep(RANDOM.nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String sayHello() {
        pre();
        String result = "hello";
        System.out.println(result);
        return result;
    }

    public String sayCold() {
        pre();
        String result = "cold";
        System.out.println(result);
        return result;
    }

    public String sayHot() {
        pre();
        String result = "hot";
        System.out.println(result);
        return result;
    }
}
