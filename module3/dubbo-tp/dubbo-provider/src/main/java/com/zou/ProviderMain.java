package com.zou;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;

public class ProviderMain {
    public static void main(String[] args) throws IOException {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ProviderConfig.class);
        System.out.println("provider start success");
        System.in.read();
    }

    @Configuration
    @EnableDubbo(scanBasePackages = "com.zou.service.impl")
    @PropertySource("classpath:/dubbo-provider.properties")
    public static class ProviderConfig {
    }
}
