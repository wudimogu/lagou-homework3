package com.zou.service;

public interface HelloService {
    public String sayHello();

    public String sayCold();

    public String sayHot();
}
