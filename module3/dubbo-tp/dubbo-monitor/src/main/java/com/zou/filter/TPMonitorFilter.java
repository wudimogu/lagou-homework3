package com.zou.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 在消费端记录每个方法的请求耗时时间（异步调用不进行记录）
 * 每隔5s打印一次最近1分钟内每个方法的TP90、TP99的耗时情况
 */
@Activate(group = CommonConstants.CONSUMER)
public class TPMonitorFilter implements Filter, Runnable {
    // 缓存的请求时长
    private static final Map<String, Map<Long, Integer>> TP_MONITOR_MAP = new HashMap<>();

    public TPMonitorFilter() {
        Executors.newSingleThreadScheduledExecutor().
                scheduleWithFixedDelay(this, 5, 5, TimeUnit.SECONDS);
    }

    /**
     * 拦截方法-添加请求耗时
     */
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        String methodName = invocation.getMethodName();

        long startTime = System.currentTimeMillis();
        Result result = invoker.invoke(invocation);
        long endTime = System.currentTimeMillis();

        int responseTime = (int) (endTime - startTime);
        // 保存请求耗时
        if (!TP_MONITOR_MAP.containsKey(methodName)) {
            // 添加一个有序的map，以添加时间排序
            TP_MONITOR_MAP.put(invocation.getMethodName(), new LinkedHashMap<>());
        }
        TP_MONITOR_MAP.get(methodName).put(startTime, responseTime);
        return result;
    }

    /**
     * 每隔5s(5000毫秒)打印一次最近1分钟内每个方法的TP90、TP99的耗时情况
     *
     * 每次数据大概（5000/30）167条
     */
    @Override
    public void run() {
        // 当前时间
        long currentTime = System.currentTimeMillis();
        // 循环现有数据
        for (Map.Entry<String, Map<Long, Integer>> entry : TP_MONITOR_MAP.entrySet()) {
            String methodName = entry.getKey();
            // 每个method的耗时
            Iterator<Map.Entry<Long, Integer>> iterator = entry.getValue().entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<Long, Integer> methodEntry = iterator.next();
                if (currentTime - methodEntry.getKey() > 60000) {
                    // 超过1分钟的数据删除
                    iterator.remove();
                } else {
                    break;
                }
            }
            // System.out.println("原件数据" + entry.getValue().toString());
            List<Integer> responseTimeList = new ArrayList<>(entry.getValue().values());
            int size = responseTimeList.size();
            // 排序
            Collections.sort(responseTimeList);
            System.out.println("排序后数据长度" + responseTimeList.size());
            // TP90
            System.out.println("-----method:" + methodName + "的TP90耗时情况:" + responseTimeList.get((int) (size * 0.9)));
            // TP 99
            System.out.println("-----method:" + methodName + "的TP99耗时情况:" + responseTimeList.get((int) (size * 0.99)));
        }
    }
}
