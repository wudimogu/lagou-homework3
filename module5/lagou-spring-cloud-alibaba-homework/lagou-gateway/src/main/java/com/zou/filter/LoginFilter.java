package com.zou.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.PathContainer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.pattern.PathPattern;
import org.springframework.web.util.pattern.PathPatternParser;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/**
 * 未登录拦截
 */
@Component
public class LoginFilter implements GlobalFilter, Ordered {

    // 网关白名单
    public static final List<PathPattern> WHITE_LIST = new ArrayList<>();

    static {
        PathPatternParser defaultInstance = PathPatternParser.defaultInstance;
        WHITE_LIST.add(defaultInstance.parse("/code/create/{email}"));
        WHITE_LIST.add(defaultInstance.parse("/user/register/{email}/{password}/{code}"));
        WHITE_LIST.add(defaultInstance.parse("/user/login/{email}/{password}"));
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        // /user/login/784890415@qq.com/123
        String path = request.getPath().toString();
        PathContainer pathContainer = PathContainer.parsePath(path);

        // 开始匹配uri
        boolean match = false;
        for (PathPattern pathPattern : WHITE_LIST) {
            if (pathPattern.matches(pathContainer)) {
                match = true;
            }
        }

        // 不匹配则开始拦截登陆，这边简单做一版，正确的应该需要检查token是否正确
        if (!match) {
            List<HttpCookie> tokenList = request.getCookies().get("token");
            if (CollectionUtils.isEmpty(tokenList)) {
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.writeWith(Mono.just(response.bufferFactory().wrap("Request be denied!".getBytes())));
            }
        }

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
