package com.zou.controller;

import com.zou.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Email;

@RestController
@RequestMapping("/user")
@RefreshScope
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 注册接⼝
     */
    @GetMapping("/register/{email}/{password}/{code}")
    public Integer register(@PathVariable String email, @PathVariable String password, @PathVariable String code) {
        return userService.register(email, password, code);
    }

    /**
     * 登录接⼝（⽣成token并⼊库，token写⼊cookie中）
     */
    @GetMapping("/login/{email}/{password}")
    public String login(@Email @PathVariable String email, @PathVariable String password,
                      HttpServletResponse response) {
        String token = userService.login(email, password);
        Cookie cookie = new Cookie("token", token);
        cookie.setPath("/");
        response.addCookie(cookie);
        return token;
    }
}
