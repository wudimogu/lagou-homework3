package com.zou.service.impl;

import com.zou.mapper.UserMapper;
import com.zou.mapper.UserTokenMapper;
import com.zou.pojo.User;
import com.zou.pojo.UserToken;
import com.zou.service.UserAuthCodeDubboService;
import com.zou.service.UserService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @DubboReference
    private UserAuthCodeDubboService userAuthCodeDubboService;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserTokenMapper userTokenMapper;

    @Override
    public Integer register(String email, String password, String code) {
        User user = getUserByEmail(email);
        if (user != null) {
            logger.info("该账号已存在");
            return 1;
        }
        Integer resultCode = userAuthCodeDubboService.validateAuthCodeByEmail(email, code);
        if (resultCode == 0) {
            user = new User()
                    .setEmail(email)
                    .setPassword(password);
            userMapper.save(user);
        }
        return resultCode;
    }

    @Override
    public String login(String email, String password) {
        User user = getUserByEmail(email);
        if (user == null) {
            return "false";
            // throw new RuntimeException("该账号不存在");
        }
        if (!user.getPassword().equals(password)) {
            return "false";
            // throw new RuntimeException("您的密码不正确");
        }
        userTokenMapper.deleteByEmail(email);
        UserToken userToken = new UserToken()
                .setEmail(email)
                .setToken(UUID.randomUUID().toString());
        userTokenMapper.save(userToken);
        return userToken.getToken();
    }

    /**
     * 通过邮件地址获取账号
     */
    private User getUserByEmail(String email) {
        User param = new User()
                .setEmail(email);
        Optional<User> tokenOptional = userMapper.findOne(Example.of(param));
        return tokenOptional.orElse(null);
    }
}
