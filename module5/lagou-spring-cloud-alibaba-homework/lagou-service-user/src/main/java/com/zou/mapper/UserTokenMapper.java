package com.zou.mapper;

import com.zou.pojo.UserToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserTokenMapper extends JpaRepository<UserToken, Long> {

    @Transactional
    void deleteByEmail(String email);
}
