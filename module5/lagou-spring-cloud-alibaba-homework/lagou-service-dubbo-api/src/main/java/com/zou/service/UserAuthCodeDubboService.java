package com.zou.service;

public interface UserAuthCodeDubboService {
    Integer validateAuthCodeByEmail(String email, String code);
}
