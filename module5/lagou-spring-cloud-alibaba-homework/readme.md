项目使用到是jia包版本
spring.cloud.version=2020.0.4
spring.cloud.alibaba.version=2.2.7.RELEASE
nacos.version=2.0.3(需要使用2.x版本)
zipkin-server.version=2.23.16(推荐使用官网的直接使用jar包方式启动 https://zipkin.io/pages/quickstart.html)
sentinel-dashboard=1.8.3