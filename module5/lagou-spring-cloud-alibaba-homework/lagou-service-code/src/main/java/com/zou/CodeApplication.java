package com.zou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
// @EnableCircuitBreaker 该注解以过时，直接使用注解@EnableHystrix（他继承了@EnableCircuitBreaker）
@EnableFeignClients(basePackages = {"com.zou.feign"})
public class CodeApplication {
    public static void main(String[] args) {
        SpringApplication.run(CodeApplication.class, args);
    }
}
