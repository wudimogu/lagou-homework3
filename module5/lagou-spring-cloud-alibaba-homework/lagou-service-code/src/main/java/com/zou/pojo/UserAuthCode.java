package com.zou.pojo;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "lagou_auth_code")
public class UserAuthCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String email;
    private String code;
    private LocalDateTime createtime;
    private LocalDateTime expiretime;

    public Integer getId() {
        return id;
    }

    public UserAuthCode setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserAuthCode setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getCode() {
        return code;
    }

    public UserAuthCode setCode(String code) {
        this.code = code;
        return this;
    }

    public LocalDateTime getCreatetime() {
        return createtime;
    }

    public UserAuthCode setCreatetime(LocalDateTime createtime) {
        this.createtime = createtime;
        return this;
    }

    public LocalDateTime getExpiretime() {
        return expiretime;
    }

    public UserAuthCode setExpiretime(LocalDateTime expiretime) {
        this.expiretime = expiretime;
        return this;
    }

    @Override
    public String toString() {
        return "UserAuthCode{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", code='" + code + '\'' +
                ", createtime=" + createtime +
                ", expiretime=" + expiretime +
                '}';
    }
}
