package com.zou.mapper;

import com.zou.pojo.UserAuthCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuthCodeMapper extends JpaRepository<UserAuthCode, Long> {

    UserAuthCode findFirstByEmailOrderByExpiretimeDesc(String email);
}
