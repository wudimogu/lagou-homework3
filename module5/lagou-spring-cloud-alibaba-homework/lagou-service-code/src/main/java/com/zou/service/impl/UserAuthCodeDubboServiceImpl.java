package com.zou.service.impl;

import com.zou.mapper.UserAuthCodeMapper;
import com.zou.pojo.UserAuthCode;
import com.zou.service.UserAuthCodeDubboService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

@Service
public class UserAuthCodeDubboServiceImpl implements UserAuthCodeDubboService {

    @Autowired
    private UserAuthCodeMapper userAuthCodeMapper;

    @Override
    public Integer validateAuthCodeByEmail(String email, String code) {
        UserAuthCode userAuthCode = userAuthCodeMapper.findFirstByEmailOrderByExpiretimeDesc(email);
        if (userAuthCode == null || userAuthCode.getExpiretime().compareTo(LocalDateTime.now()) < 0 || !userAuthCode.getCode().equals(code)) {
            return 1;
        }
        return 0;
    }
}
