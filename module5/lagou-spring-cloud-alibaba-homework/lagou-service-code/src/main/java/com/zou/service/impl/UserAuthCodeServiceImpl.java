package com.zou.service.impl;

import com.zou.mapper.UserAuthCodeMapper;
import com.zou.pojo.UserAuthCode;
import com.zou.service.MailDubboService;
import com.zou.service.UserAuthCodeService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.time.LocalDateTime;
import java.util.Random;

@Service
public class UserAuthCodeServiceImpl implements UserAuthCodeService {

    @Autowired
    private UserAuthCodeMapper userAuthCodeMapper;
    @DubboReference
    private MailDubboService mailDubboService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean createCodeByEmail(String email) {
        UserAuthCode userAuthCode = userAuthCodeMapper.findFirstByEmailOrderByExpiretimeDesc(email);
        LocalDateTime now = LocalDateTime.now();
        if (userAuthCode != null && userAuthCode.getExpiretime().compareTo(now) >= 0) {
            // throw new RuntimeException("请不要重复发送验证码");
            return false;
        }

        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            sb.append(random.nextInt(10));
        }

        userAuthCode = new UserAuthCode()
                .setEmail(email)
                .setCode(sb.toString())
                .setCreatetime(now)
                // 一分钟内只允许获取一次验证码
                .setExpiretime(now.plusMinutes(1));
        userAuthCodeMapper.save(userAuthCode);
        // 启动邮件服务发送验证码
        Boolean emailing = mailDubboService.email(email, userAuthCode.getCode());
        if (!emailing) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }
}
