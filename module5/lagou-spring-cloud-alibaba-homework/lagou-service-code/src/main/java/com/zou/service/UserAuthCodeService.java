package com.zou.service;

public interface UserAuthCodeService {
    Boolean createCodeByEmail(String email);
}
