package com.zou.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@RestController
public class MailController {

    @Autowired
    private JavaMailSender mailSender;

    @GetMapping("/email/{email}/{code}")
    public Boolean email(@PathVariable String email, @PathVariable String code) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        try {
            helper.setFrom("784890415@qq.com");
            helper.setTo(email);
            helper.setSubject("验证码短信邮件");
            helper.setText("您的验证码为" + code);
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }

         mailSender.send(mimeMessage);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }
}
