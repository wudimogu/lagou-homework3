import com.zou.EmailApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = EmailApplication.class)
public class MailTest {

    @Autowired
    private JavaMailSender mailSender;

    @Test
    public void test() throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setFrom("784890415@qq.com");
        helper.setTo("784890415@qq.com");
        helper.setSubject("验证码短信邮件");
        helper.setText("您的验证码为12345");

        mailSender.send(mimeMessage);
    }
}
