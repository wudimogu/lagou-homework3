package com.zou.service;

public interface UserService {
    Integer register(String email, String password, String code);

    String login(String email, String password);
}
