package com.zou.feign;

import com.zou.feign.impl.AuthCodeFeignFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "lagou-service-code", fallback = AuthCodeFeignFallBack.class)
public interface AuthCodeFeign {
    @GetMapping("/code/validate/{email}/{code}")
    Integer validateCode(@PathVariable("email") String email, @PathVariable("code") String code);
}
