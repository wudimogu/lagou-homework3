package com.zou.feign.impl;

import com.zou.feign.AuthCodeFeign;
import org.springframework.stereotype.Component;

@Component
public class AuthCodeFeignFallBack implements AuthCodeFeign {
    @Override
    public Integer validateCode(String email, String code) {
        return 2;
    }
}
