package com.zou.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Date;

/**
 * 自定义IP防暴刷指标过滤器
 */
@Component
public class FixedWindowFilter implements GlobalFilter, Ordered {

    private long time = new Date().getTime();
    // 计数器
    private Integer count = 0;
    // 请求阈值
    @Value("${spring.cloud.gateway.rate-limiter.max}")
    private final Integer max = 100;
    // 窗口大小(固定窗格)
    @Value("${spring.cloud.gateway.rate-limiter.interval}")
    private final Integer interval = 1000;

    public FixedWindowFilter() {
        System.out.println(max);
        System.out.println(interval);
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if (!trafficMonitoring()) {
            ServerHttpResponse response = exchange.getResponse();
            response.setStatusCode(HttpStatus.SEE_OTHER);
            return response.writeWith(Mono.just(response.bufferFactory().wrap(("rate limit:there can only be " + max + " requests every " + interval + "ms").getBytes())));
        }
        return chain.filter(exchange);
    }

    /**
     * 时间窗内的限流算法
     */
    public boolean trafficMonitoring() {
        long nowTime = new Date().getTime();
        if (nowTime < time + interval) {
            // 在时间窗口内
            count++;
            return max > count;
        } else {
            // 开启新的窗口
            time = nowTime;
            // 初始化计数器,由于这个请求属于当前新开的窗口，所以记录这个请求
            count = 1;
            return true;
        }
    }

    @Override
    public int getOrder() {
        return 100;
    }
}
