//package com.zou.config;
//
//import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import reactor.core.publisher.Mono;
//
//@Configuration
//public class RequestRateLimitConfig {
//    /**
//     * IP限流：
//     * 获取请求用户ip作为限流key。
//     */
//    @Bean
//    public KeyResolver hostAddrKeyResolver() {
//        return exchange -> Mono.just(
//                exchange.getRequest().getRemoteAddress().getHostName());
//    }
//
//}
