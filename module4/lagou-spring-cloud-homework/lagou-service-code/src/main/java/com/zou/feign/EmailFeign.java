package com.zou.feign;

import com.zou.feign.impl.EmailFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "lagou-service-email", fallback = EmailFeignFallback.class)
public interface EmailFeign {
    @GetMapping("/email/{email}/{code}")
    Boolean email(@PathVariable(value = "email") String email, @PathVariable(value = "code") String code);
}
