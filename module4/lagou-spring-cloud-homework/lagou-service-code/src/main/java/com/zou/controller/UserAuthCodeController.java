package com.zou.controller;

import com.zou.pojo.UserAuthCode;
import com.zou.service.UserAuthCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Email;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/code")
public class UserAuthCodeController {

    private static final Logger logger = LoggerFactory.getLogger(UserAuthCodeController.class);

    @Autowired
    private UserAuthCodeService userAuthCodeService;

    @GetMapping("/create/{email}")
    public Boolean getCode(@Email @PathVariable String email) {
        return userAuthCodeService.createCodeByEmail(email);
    }

    @GetMapping("/validate/{email}/{code}")
    public Integer validateCode(@PathVariable("email") String email, @PathVariable("code") String code) {
        UserAuthCode userAuthCode = userAuthCodeService.getAuthCodeByEmail(email);
        if (userAuthCode == null || userAuthCode.getExpiretime().compareTo(LocalDateTime.now()) < 0 || !userAuthCode.getCode().equals(code)) {
            logger.info("验证码有误");
            return 1;
        }
        return 0;
    }
}
