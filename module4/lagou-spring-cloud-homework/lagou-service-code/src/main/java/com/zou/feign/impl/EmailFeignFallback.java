package com.zou.feign.impl;

import com.zou.feign.EmailFeign;
import org.springframework.stereotype.Component;

@Component
public class EmailFeignFallback implements EmailFeign {
    @Override
    public Boolean email(String email, String code) {
        return false;
    }
}
