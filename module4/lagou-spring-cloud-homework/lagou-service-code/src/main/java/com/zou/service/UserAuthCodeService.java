package com.zou.service;

import com.zou.pojo.UserAuthCode;

public interface UserAuthCodeService {
    Boolean createCodeByEmail(String email);

    UserAuthCode getAuthCodeByEmail(String email);
}
