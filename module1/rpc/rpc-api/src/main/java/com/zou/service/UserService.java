package com.zou.service;

import com.zou.entity.User;

public interface UserService {

    User getById(int id);
}
