package com.zou.serviceImpl;

import com.zou.anno.RPCService;
import com.zou.entity.User;
import com.zou.service.UserService;
import org.springframework.stereotype.Service;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

@Service
@RPCService
public class UserServiceImpl implements UserService {

    // 用户数据列表
    private final Map<Integer, User> userMap = new HashMap<>();

    public UserServiceImpl() throws RemoteException {
        super();
        User user1 = new User(1, "张三");
        userMap.put(user1.getId(), user1);
        User user2 = new User(2, "李四");
        userMap.put(user2.getId(), user2);
    }

    @Override
    public User getById(int id) {
        return userMap.get(id);
    }
}
