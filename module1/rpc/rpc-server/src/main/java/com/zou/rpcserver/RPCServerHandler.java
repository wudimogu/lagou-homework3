package com.zou.rpcserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zou.anno.RPCService;
import com.zou.rpc.Request;
import com.zou.rpc.Response;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.springframework.beans.BeansException;
import org.springframework.cglib.reflect.FastClass;
import org.springframework.cglib.reflect.FastMethod;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 自定义RPC Server handler相关使用
 */
@Component
@ChannelHandler.Sharable
public class RPCServerHandler extends SimpleChannelInboundHandler<String> implements ApplicationContextAware {

    // 远程对象集合
    private final Map<String, Object> remoteContext = new ConcurrentHashMap<>();

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("通道就绪");
    }

    /**
     * 自定义read通道事件
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Request request = objectMapper.readValue(msg, Request.class);
        // 获取远程对象
        Object remote = remoteContext.get(request.getClassName());
        // 通过反射调用方法-cglib
        FastClass aClass = FastClass.create(remote.getClass());
        FastMethod method = aClass.getMethod(request.getMethodName(), request.getParameterTypes());
        Object result = method.invoke(remote, request.getParameterValues());
        // 输出消息
        Response response = new Response();
        response.setResult(result);
        ctx.writeAndFlush(objectMapper.writeValueAsString(response));
        System.out.println("服务器返回消息");
    }

    /**
     * 使用spring上下文
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        // 获取远程对象
        Map<String, Object> RPCServiceMap = applicationContext.getBeansWithAnnotation(RPCService.class);
        // 注册远程对象
        for (Map.Entry<String, Object> RPCService : RPCServiceMap.entrySet()) {
            remoteContext.put(RPCService.getValue().getClass().getInterfaces()[0].getName(), RPCService.getValue());
        }
    }
}
