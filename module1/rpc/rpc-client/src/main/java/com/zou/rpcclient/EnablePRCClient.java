package com.zou.rpcclient;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(RPCClientRegister.class)
public @interface EnablePRCClient {
    // 所在包
    String[] basePackages() default {};
}
