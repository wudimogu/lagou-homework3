package com.zou.rpcclient;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.concurrent.Callable;

public class RPCClientHandler extends SimpleChannelInboundHandler<String> implements Callable<String> {

    private ChannelHandlerContext context;

    private String requestMsg;
    private String responseMsg;

    /**
     * 通道连接就绪事件
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        context = ctx;
        System.out.println("通道就绪");
    }

    /**
     * 通道读取就绪事件
     */
    @Override
    protected synchronized void channelRead0(ChannelHandlerContext channelHandlerContext, String msg) throws Exception {
        responseMsg = msg;
        //唤醒等待的线程
        notify();
    }

    /**
     * 发送消息到服务端
     */
    @Override
    public synchronized String call() throws Exception {
        //消息发送
        context.writeAndFlush(requestMsg);
        //线程等待
        wait();
        return responseMsg;
    }

    public void setRequestMsg(String requestMsg) {
        this.requestMsg = requestMsg;
    }

}
