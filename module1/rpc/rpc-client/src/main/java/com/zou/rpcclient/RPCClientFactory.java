package com.zou.rpcclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zou.rpc.Request;
import com.zou.rpc.Response;
import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 使用spring FactoryBean
 */
public class RPCClientFactory<T> implements FactoryBean<T> {

    private final Class<T> RPCService;
    private final List<String> serverAddrList = new ArrayList<>();
    // 当前服务器下标
    private static final AtomicInteger SERVER_INDEX = new AtomicInteger(0);

    public RPCClientFactory(Class<T> RPCService, String serverAddr) {
        this.RPCService = RPCService;
        String[] split = serverAddr.split(",");
        Collections.addAll(serverAddrList, split);
    }

    /**
     * 代理增强
     */
    @Override
    public T getObject() throws Exception {
        return (T) getProxy(RPCService);
    }

    @Override
    public Class<?> getObjectType() {
        return RPCService;
    }

    /**
     * 使用jdk代理
     */
    public Object getProxy(Class<?> aClass) {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{aClass}, new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        // 具体的增强方法
                        // 1.封装请求
                        Request request = new Request();
                        request.setRequestId(UUID.randomUUID().toString());
                        request.setClassName(aClass.getName());
                        request.setMethodName(method.getName());
                        request.setParameterTypes(method.getParameterTypes());
                        request.setParameterValues(args);
                        RPCClient rpcClient = null;
                        try {
                            String serverAddr = roundRobin();
                            String[] server = serverAddr.split(":");

                            System.out.println("开始请求服务器" + serverAddr);
                            rpcClient = new RPCClient(server[0], Integer.parseInt(server[1]));
                            // 3.发送请求
                            ObjectMapper objectMapper = new ObjectMapper();
                            String result = rpcClient.send(objectMapper.writeValueAsString(request));
                            // 4.数据转换
                            Response response = objectMapper.readValue(result, Response.class);
                            // 5.返回数据
                            return objectMapper.readValue(objectMapper.writeValueAsString(response.getResult()), method.getReturnType());
                        } catch (Exception e) {
                            e.printStackTrace();
                            throw e;
                        } finally {
                            rpcClient.destroy();
                        }
                    }
                });
    }

    /**
     * 简单轮询算法
     */
    private synchronized String roundRobin() {
        if (SERVER_INDEX.get() >= serverAddrList.size()) {
            SERVER_INDEX.set(0);
        }
        return serverAddrList.get(SERVER_INDEX.getAndIncrement());
    }
}
