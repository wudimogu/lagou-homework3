package com.zou.rpcclient;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.springframework.beans.factory.DisposableBean;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RPCClient implements DisposableBean {

    private ChannelFuture channelFuture;
    private NioEventLoopGroup nioEventLoopGroup;
    private final ExecutorService executorService;

    private final RPCClientHandler rpcClientHandler = new RPCClientHandler();

    public RPCClient(String host, int port) {
        start(host, port);
        executorService = Executors.newFixedThreadPool(2);
    }

    /**
     * 开启netty
     */
    public void start(String host, int port) {
        try {
            nioEventLoopGroup = new NioEventLoopGroup();
            // 启动助手
            Bootstrap bootstrap = new Bootstrap()
                    .group(nioEventLoopGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, Boolean.TRUE)
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 3000)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            // 注入String编解码器
                            ch.pipeline().addLast(new StringEncoder());
                            ch.pipeline().addLast(new StringDecoder());
                            // 注入handler
                            ch.pipeline().addLast(rpcClientHandler);
                        }
                    });
            // 开启监听
            channelFuture = bootstrap.connect(host, port).sync();
        } catch (Exception e) {
            e.printStackTrace();
            destroy();
        }
    }

    /**
     * 发送消息
     */
    public String send(String msg) throws ExecutionException, InterruptedException {
        rpcClientHandler.setRequestMsg(msg);
        Future<String> submit = executorService.submit(rpcClientHandler);
        return submit.get();
    }

    @Override
    public void destroy() {
        channelFuture.channel().close();
        nioEventLoopGroup.shutdownGracefully();
    }
}
