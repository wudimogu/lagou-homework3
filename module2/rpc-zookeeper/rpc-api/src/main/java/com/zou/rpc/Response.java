package com.zou.rpc;

public class Response {
    private String requestId;

    private Object result;

    public String getRequestId() {
        return requestId;
    }

    public Response setRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    public Object getResult() {
        return result;
    }

    public Response setResult(Object result) {
        this.result = result;
        return this;
    }

    @Override
    public String toString() {
        return "Response{" +
                "requestId='" + requestId + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
