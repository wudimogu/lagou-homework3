package com.zou.rpcserver;

import com.zou.rpc.RPCCommon;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

@Component
public class RPCServer implements DisposableBean, ExitCodeGenerator {

    private NioEventLoopGroup bossGroup;
    private NioEventLoopGroup workerGroup;

    @Autowired
    private RPCServerHandler rpcServerHandler;

    private CuratorFramework client;

    public void start(int port, String zkAddr) {
        try {
            bossGroup = new NioEventLoopGroup(1);
            workerGroup = new NioEventLoopGroup();

            // 助手
            ServerBootstrap serverBootstrap = new ServerBootstrap()
                    .group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            // 使用String编解码器-使用json
                            pipeline.addLast(new StringDecoder());
                            pipeline.addLast(new StringEncoder());
                            // 业务处理类
                            pipeline.addLast(rpcServerHandler);
                        }
                    });
            // 开启绑定
            ChannelFuture channelFuture = serverBootstrap.bind(port).sync();
            System.out.println("RPCServer start on " + port + " success...");
            // 注册zk
            registerZK(zkAddr, port);
            // 监听关闭
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            destroy();
        }
    }

    /**
     * 注册zookeeper
     */
    public void registerZK(String zkAddr, int port) {
        try {
            // 创建链接
            client = CuratorFrameworkFactory.builder()
                    .connectString(zkAddr)
                    .namespace(RPCCommon.ZK_NAMESPACE)
                    .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                    .build();
            client.start();
            // 创建节点
            String hostAddress = InetAddress.getLocalHost().getHostAddress();
            client.create().withMode(CreateMode.EPHEMERAL).forPath("/" + hostAddress + ":" + port, "-1".getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        System.out.println("exit");
        if (bossGroup != null) {
            bossGroup.shutdownGracefully();
        }
        if (workerGroup != null) {
            workerGroup.shutdownGracefully();
        }
        if (client != null) {
            client.close();
        }
    }

    @Override
    public int getExitCode() {
        return 5;
    }
}
