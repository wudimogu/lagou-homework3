package com.zou.rpcclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zou.rpc.RPCCommon;
import com.zou.rpc.Request;
import com.zou.rpc.Response;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 使用spring FactoryBean
 */
public class RPCClientFactory<T> implements FactoryBean<T> {

    private static final Logger logger = LoggerFactory.getLogger(RPCClientFactory.class);

    private final Class<T> RPCService;
    // 当前服务器下标
    private static final AtomicInteger SERVER_INDEX = new AtomicInteger(0);
    // zk
    private CuratorFramework client;
    // server地址
    private Map<String, serverResponseTime> serverAddrMap = new ConcurrentHashMap<>();
    // private List<String> serverAddrList;
    // 定时上报线程池
    private final ScheduledExecutorService executorService;

    public RPCClientFactory(Class<T> RPCService, String zkAddr) {
        this.RPCService = RPCService;
        // 创建zk链接
        initZK(zkAddr);
        // 定时上报线程池
        executorService = Executors.newScheduledThreadPool(2);
        // 1.2 消费者定时在启动是创建定时线程池，每隔5s自动上报，更新zk节点值
        executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    // 1.3 上报时判断当前事件距离最后一次请求是否超过5s，超过则重置zk节点的值
                    logger.info("*********开始上报服务端响应时长");
                    // 上报所有服务器响应时长
                    for (Map.Entry<String, serverResponseTime> entry : serverAddrMap.entrySet()) {
                        if (entry.getValue().getResponseTime() > 0) {
                            Integer responseTime = entry.getValue().getResponseTime();
                            // 距离最后一次请求超过5s，超过则重置zk节点的值
                            if ((System.currentTimeMillis() - entry.getValue().getCurrentTime()) / 1000 > 5) {
                                responseTime = -1;
                                entry.setValue(entry.getValue().setResponseTime(-1));
                            }
                            client.setData().forPath("/" + entry.getKey(), responseTime.toString().getBytes());
                            logger.info("*********上报服务器" + entry.getKey() + "的响应时长为：" + entry.getValue());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 5, 5, TimeUnit.SECONDS);
    }

    /**
     * 初始化zk链接并启动
     */
    private void initZK(String zkAddr) {
        try {
            this.client = CuratorFrameworkFactory.builder()
                    .connectString(zkAddr)
                    .namespace(RPCCommon.ZK_NAMESPACE)
                    .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                    .build();
            client.start();
            // 添加监听
            PathChildrenCache pathChildrenCache = new PathChildrenCache(client, "/", true);
            pathChildrenCache.getListenable().addListener(new PathChildrenCacheListener() {
                /**
                 * 监听回调
                 */
                @Override
                public void childEvent(CuratorFramework client, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
                    // 查询所有子节点-同步节点
                    convertNode(client);
                }
            });
            pathChildrenCache.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * node list转换成map
     */
    private void convertNode(CuratorFramework client) throws Exception {
        List<String> nodeList = client.getChildren().forPath("/");
        // 同步节点
        for (String serverAddr : serverAddrMap.keySet()) {
            if (!nodeList.contains(serverAddr)) {
                // 删除
                serverAddrMap.remove(serverAddr);
            } else {
                // 同步响应时长-已事件长的为准
                int responseTime = Integer.parseInt(new String(client.getData().forPath("/" + serverAddr)));
                if (responseTime > serverAddrMap.get(serverAddr).getResponseTime()) {
                    serverAddrMap.put(serverAddr, new serverResponseTime(responseTime, System.currentTimeMillis()));
                }
            }
            nodeList.remove(serverAddr);
        }
        // 新增节点
        for (String node : nodeList) {
            serverAddrMap.put(node, new serverResponseTime(-1, System.currentTimeMillis()));
        }
        StringBuilder sb = new StringBuilder();
        for (String serverAddr : serverAddrMap.keySet()) {
            sb.append(serverAddr + ",");
        }
        logger.info("服务点节点发生变化，当前共有节点:" + sb.toString());
    }

    /**
     * 代理增强
     */
    @Override
    public T getObject() throws Exception {
        return (T) getProxy(RPCService);
    }

    @Override
    public Class<?> getObjectType() {
        return RPCService;
    }

    /**
     * 使用jdk代理
     */
    public Object getProxy(Class<?> aClass) {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{aClass}, new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        // 具体的增强方法
                        // 1.封装请求
                        Request request = new Request();
                        request.setRequestId(UUID.randomUUID().toString());
                        request.setClassName(aClass.getName());
                        request.setMethodName(method.getName());
                        request.setParameterTypes(method.getParameterTypes());
                        request.setParameterValues(args);
                        RPCClient rpcClient = null;
                        try {
                            // String serverAddr = roundRobinAlgorithm();
                            String serverAddr = responseTimeAlgorithm();
                            String[] server = serverAddr.split(":");

                            rpcClient = new RPCClient(server[0], Integer.parseInt(server[1]));
                            // 3.发送请求
                            ObjectMapper objectMapper = new ObjectMapper();
                            long startTime = System.currentTimeMillis();
                            String result = rpcClient.send(objectMapper.writeValueAsString(request));
                            long endTime = System.currentTimeMillis();
                            int responseTime = (int) ((endTime - startTime) / 1000);
                            // 1.1 消费者每次请求完成后更新最后一次请求耗时和系统事件
                            logger.info("开始请求服务器" + serverAddr + "，本次响应时长为：" + responseTime);
                            serverAddrMap.put(serverAddr, new serverResponseTime(responseTime, System.currentTimeMillis()));
                            // 4.数据转换
                            Response response = objectMapper.readValue(result, Response.class);
                            // 5.返回数据
                            return objectMapper.readValue(objectMapper.writeValueAsString(response.getResult()), method.getReturnType());
                        } catch (Exception e) {
                            e.printStackTrace();
                            throw e;
                        } finally {
                            rpcClient.destroy();
                        }
                    }
                });
    }

    /**
     * 简单轮询算法
     */
    private synchronized String roundRobinAlgorithm() {
        if (SERVER_INDEX.get() >= serverAddrMap.size()) {
            SERVER_INDEX.set(0);
        }
        return (String) serverAddrMap.keySet().toArray()[SERVER_INDEX.getAndIncrement()];
    }

    /**
     * 简单响应时长算法
     */
    private synchronized String responseTimeAlgorithm() {
        logger.info("-----------开始执行简单响应时长算法");
        String serverAddr = null;
        Integer responseTime = null;
        for (Map.Entry<String, serverResponseTime> entry : serverAddrMap.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue().getResponseTime();
            logger.info("-----------服务器" + key + "的响应时长为" + value);
            if (responseTime == null) {
                serverAddr = key;
                responseTime = value;
            }
            if (value < responseTime) {
                serverAddr = key;
                responseTime = value;
            }
        }
        logger.info("-----------最优服务器为" + serverAddr);
        return serverAddr;
    }

    public static class serverResponseTime {
        // 响应事件
        private Integer responseTime;
        // 当前时间
        private Long currentTime;

        public serverResponseTime(Integer responseTime, Long currentTime) {
            this.responseTime = responseTime;
            this.currentTime = currentTime;
        }

        public Integer getResponseTime() {
            return responseTime;
        }

        public serverResponseTime setResponseTime(Integer responseTime) {
            this.responseTime = responseTime;
            return this;
        }

        public Long getCurrentTime() {
            return currentTime;
        }

        public serverResponseTime setCurrentTime(Long currentTime) {
            this.currentTime = currentTime;
            return this;
        }

        @Override
        public String toString() {
            return "serverResponseTime{" +
                    "responseTime=" + responseTime +
                    ", currentTime=" + currentTime +
                    '}';
        }
    }
}
