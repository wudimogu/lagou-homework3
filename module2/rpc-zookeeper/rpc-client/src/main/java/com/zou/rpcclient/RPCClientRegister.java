package com.zou.rpcclient;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 通过配置类 向ioc容器注入remote类
 */
public class RPCClientRegister implements BeanDefinitionRegistryPostProcessor, ResourceLoaderAware, ApplicationContextAware {

    private static final String DEFAULT_RESOURCE_PATTERN = "**/*.class";

    private ApplicationContext applicationContext;
    private ResourcePatternResolver resourcePatternResolver;
    private MetadataReaderFactory metadataReaderFactory;

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
        System.out.println("postProcessBeanDefinitionRegistry");
        try {
            // todo 获取扫描路径-如何能获取到@Import中的属性
//            MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(RpcClientApplication.class.getCanonicalName());
//            AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
//            Map<String, Object> annotationAttributes =
//                    annotationMetadata.getAnnotationAttributes(EnablePRCClient.class.getName());
//            String basePackages = (String) annotationAttributes.get("basePackages");
            // 暂时写死
            String basePackages = "com.zou.service";
            // 获取扫描类
            Set<Class<?>> beanClass = scannerPackages(basePackages);
            for (Class<?> aClass : beanClass) {
                // BeanDefinition构造器
                BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(aClass);
                GenericBeanDefinition beanDefinition = (GenericBeanDefinition) builder.getRawBeanDefinition();
                // 给该对象的属性注入对应的实例
//                beanDefinition.getConstructorArgumentValues().addGenericArgumentValue(aClass);
                // 如果构造方法不止一个，可以使用这个api,指定是第几个参数
                ConstructorArgumentValues constructorArgumentValues = beanDefinition.getConstructorArgumentValues();
                constructorArgumentValues.addIndexedArgumentValue(0, aClass);
                constructorArgumentValues.addIndexedArgumentValue(1, applicationContext.getEnvironment().getProperty("rpc.zk-addr"));
                // 定义FactoryBean，代理增强类
                beanDefinition.setBeanClass(RPCClientFactory.class);
                // 使用byType方式注入
                beanDefinition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
                // 注入容器
                beanDefinitionRegistry.registerBeanDefinition(lowerFirst(aClass.getSimpleName()), beanDefinition);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 扫描需要的类
     */
    private Set<Class<?>> scannerPackages(String basePackage) {
        Set<Class<?>> set = new LinkedHashSet<>();
        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                resolveBasePackage(basePackage) + '/' + DEFAULT_RESOURCE_PATTERN;
        try {
            Resource[] resources = this.resourcePatternResolver.getResources(packageSearchPath);
            for (Resource resource : resources) {
                if (resource.isReadable()) {
                    MetadataReader metadataReader = this.metadataReaderFactory.getMetadataReader(resource);
                    String className = metadataReader.getClassMetadata().getClassName();
                    Class<?> clazz;
                    try {
                        clazz = Class.forName(className);
                        set.add(clazz);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return set;
    }

    private String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(applicationContext.getEnvironment().resolveRequiredPlaceholders(basePackage));
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        System.out.println("postProcessBeanFactory");
    }

    /**
     * spring 上下文
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * 资源加载
     */
    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourcePatternResolver = ResourcePatternUtils.getResourcePatternResolver(resourceLoader);
        this.metadataReaderFactory = new CachingMetadataReaderFactory(resourceLoader);
    }

    private String lowerFirst(String str) {
        assert str != null && str.length() > 0;
        return String.valueOf(str.charAt(0)).toLowerCase() + str.substring(1);
    }
}
